export function writePortfolio() {
  return `<div class="main">
  <h1>My Stuff</h1>
  <hr />

  <!-- Portfolio Gallery Grid -->
  <div class="row">
    <div class="column">
      <div class="content">
        <img
          src="img/almost_there.jpg"
          alt="Almost There Image"
          style="width: 100%"
        />
        <h3>My Work</h3>
        <p>Lorem ipsum..</p>
      </div>
    </div>
    <div class="column">
      <div class="content">
        <img
          src="img/mountain.jpg"
          alt="Mountain"
          style="width: 100%"
        />
        <h3>My Work</h3>
        <p>Lorem ipsum..</p>
      </div>
    </div>
    <div class="column">
      <div class="content">
        <img src="img/at_top.jpg" alt="Nature" style="width: 100%" />
        <h3>My Work</h3>
        <p>Lorem ipsum..</p>
      </div>
    </div>
    <div class="column">
      <div class="content">
        <img
          src="img/wanderer.jpg"
          alt="wanderer"
          style="width: 100%"
        />
        <h3>My Work</h3>
        <p>Lorem ipsum..</p>
      </div>
    </div>
  </div>

  <div class="content">
    <img src="img/bear.jpg" alt="Bear" style="width: 100%" />
    <h3>When Hangry!</h3>
    <p>Lorem ipsum..</p>
  </div>

  <!-- END MAIN -->
</div>`;
}
